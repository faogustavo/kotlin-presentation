import { future as theme } from 'mdx-deck/themes'
import duotoneLight from "prism-react-renderer/themes/duotoneLight"

export default {
  ...theme,
  font: 'Roboto, sans-serif',
  colors: {
    ...theme.colors,
    background: '#0291d8'
  }
}
