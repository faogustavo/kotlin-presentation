import React from 'react'
import Flex from './Flex'

export default ({ children }) => (
  <Flex css={{
      flexDirection: 'column',
      alignItems: 'flex-start'
    }}>
    {children}
  </Flex>
)