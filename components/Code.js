import React from 'react'
import { CodeSurfer } from 'mdx-deck-code-surfer'
import duotoneLight from "prism-react-renderer/themes/duotoneLight"

export default ({ title, lang = "java", snippetName, steps = true }) => (
  <CodeSurfer
    title={title}
    theme={duotoneLight}
    code={require(`!raw-loader!../snippets/${snippetName}`)}
    steps={steps ? require(`../snippets/${snippetName}.json`) : null}
    lang={lang}
    showNumbers
  />
)