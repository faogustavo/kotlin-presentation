import React from 'react'

export default ({ src, height, width, size = 200, round = false }) => {
  const finalStyle = {
    borderRadius: round ? "50%" : 0
  }
  const finalHeight = width && height || size;
  const finalWidth = height && width || size;

  return (
    <img src={src} height={finalHeight} width={finalWidth} style={finalStyle} />
  )
}