import React from 'react'

import Flex from '../components/Flex'
import Split from '../components/Split'
import Left from '../components/left'
import Right from '../components/right'
import Image from '../components/image'
import Small from '../components/small'

export default () => (
  <Split left={2 / 5} spaceBetween={32}>
    <React.Fragment>
      <Right>
        <Image
          src="assets/me.jpg"
          size={150}
          round
        />
      </Right>
      <Left>
        <Flex>
          <span>Gustavo Fão Valvassori</span>
          <Small paddingLeft={12} quantity={4}>
            (@faogustavo)
        </Small>
        </Flex>
        <Small quantity={2}>Android Developer at CodeMiner42</Small>
      </Left>
    </React.Fragment>
  </Split>
)