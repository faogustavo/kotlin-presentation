interface Business {
  fun loadUsers(): List<User>
}

class AppBusiness(val database:
          DatabaseConnection): Business {
  override fun loadUsers() : List<User> {
    database.loadUsers()
  }
}

class Person(val name: String) {
  fun sayHello() {
    println("Hello, I'm $name")
  }
}

class Costumer(val name: String, val document: String)
  : Person(name) {
  fun sayDocument() {
    println("My document is $document")
  }
}