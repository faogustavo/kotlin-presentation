package domain.from.project.toolkit

import java.util.Calendar

fun Calendar.isSameDay(otherDay: Calendar) =
  get(Calendar.ERA) == otherDay.get(Calendar.ERA) &&
  get(Calendar.YEAR) == otherDay.get(Calendar.YEAR) &&
  get(Calendar.DAY_OF_YEAR) == otherDay.get(Calendar.DAY_OF_YEAR)

// Call
date1.isSameDay(date2)