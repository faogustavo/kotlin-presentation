List<String> fruits = new ArrayList();
fruits.add("banana");
fruits.add("avocado");
fruits.add("apple");
fruits.add("kiwifruit");

fruits.stream() // Java 8+
  .filter(it -> it.startsWith("a"))
  .sorted()
  .map(it -> it.toUpperCase())
  .forEach(it -> System.out.println(it))