val point: Point = Point(0, 1)

// Generally used to NullSafety
point.let {
    // this -> class instance
    // it -> point
    // Do return a value
} // Returned value

// Generally used to change data
point.run {
    // this -> point
    // it -> N/a
    // Do return a value
} // Returned value

// Generally used to apply
// multiple actions
point.apply {
    // this -> point
    // it -> N/a
    // Dont't return a value
} // Point

// Generally used to do something
// after another action
point.also {
    // this -> class Instance
    // it -> point
    // Don't return a value
} // Point

// Instead of
binding.toolbar?.title?.text = "AppTitle"
binding.toolbar?.subtitle?.hide()
binding.toolbar?.backButton?.setOnClickListener { onBackPressed() }

// do
binding?.toolbar?.apply {
    title?.text = "AppTitle"
    subtitle?.hide()
    backButton?.setOnClickListener { onBackPressed() }
}

// Returns the value if the lambda
// returns true
val intValue = 1
intValue.takeIf { it > 0 }?.apply {
    println("Positive number")
}

// or if returns false
intValue.takeUnless { it <= 0 }?.apply {
    println("Positive number")
}

// Instead of
if (point % Point.Two == Point.Zero)
    plot(point)

point.takeIf {
    it % Point.Two == Point.Zero
}?.apply { plot(this) }