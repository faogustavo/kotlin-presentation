fun numbersFromOneToTen() {
    for (i in 1..10) {
        println("Number: $i")
    }
}

fun evenNumbersFromTenToOne() {
    for (i in 10 downTo 0 steps 2) {
        println("Number: $i")
    }
}

fun fruitWhile() {
    val fruits = listOf("apple", "banana", "kiwifruit")
    var index = 0
    while (index < fruits.size) {
        println("item at $index is ${fruits[index]}")
        index++
    }
}

fun fruitLoop() {
    val fruits = listOf("apple", "banana", "kiwifruit")
    for (fruit in fruits) {
        println(fruit)
    }
}
