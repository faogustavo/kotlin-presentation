data class Client (
  val id: Long,
  val createdAt: Date,
  val updatedAt: Date,
  val active: Boolean,
  val name: String,
  val properties: List<Property>
)