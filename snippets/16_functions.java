public class StringUtils {

    public static String normalizeString(
        String str,
        Boolean normalizeCase,
        Boolean upperCaseFirstLetter
    ) {
        // normalize string
    }

    public static String normalizeString(String str) {
        return normalizeString(str, true, true);
    }

    public static String normalizeString(
        String str,
        Boolean normalizeCase
    ) {
        return normalizeString(str, normalizeCase, true);
    }

    // Error. Same parameters as the method above
    public static String normalizeString(
        String str,
        Boolean upperCaseFirstLetter
    ) {
        return normalizeString(str, true, upperCaseFirstLetter);
    }

}