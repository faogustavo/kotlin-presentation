fun externalFunction() {
    println("Call of another function")
}

inline fun runBlock(block: () -> Unit) {
    block()
}

infix fun Int.isModBy(value: Int) = this % value == 0

fun main(args: Array<String>) {
    // Functions
    externalFunction()

    // Function as parameter
    val anotherFun = ::externalFunction
    anotherFun()

    // Lambdas
    val simpleLambda: (String) -> Unit = { parameter ->
        println("simpleFun(\"$parameter\")")
    }
    simpleLambda("Hello World")

    // Lambdas with inferation
    val inferedLambda = { parameter: String ->
        println("inferedLambda(\"$parameter\")")
    }
    inferedLambda("Hello World")

    // Inline functions
    runBlock {
        println("Block called inside " +
                "other functio n")
    }

    // Infix fun
    if (4 isModBy 2) {
        println("4 is dividede by 2")
    }
}