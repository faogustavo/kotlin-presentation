public class Calculator {

    interface Expression {
        double eval();
    }

    static class Number implements Expression {
        private double value;

        public Number(double value) {
            this.value = value;
        }

        @Override
        public double eval() {
            return value;
        }
    }

    static class Sum implements Expression {
        private final Expression e1;
        private final Expression e2;

        public Sum(Expression e1, Expression e2) {
            this.e1 = e1;
            this.e2 = e2;
        }

        @Override
        public double eval() {
            return e1.eval() + e2.eval();
        }
    }

    public static void main(String[] args) {
        Expression sum = new Sum(new Number(1), new Number(2));
        System.out.println("1 + 2 = " + sum.eval());
    }
}
