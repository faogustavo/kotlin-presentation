package main

fun main(args: Array<String>) {
    println("Hello world")
}

fun declaration() {
    val a: Int = 1
    var b: Int = 2
    a += 1 // error: val cannot be reassigned
    b += 1
}