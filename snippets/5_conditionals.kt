fun maxOf(a: Int, b: Int): Int {
    if (a > b) {
        return a
    } else {
        return b
    }
}

fun singleReturnMaxOf(a: Int, b: Int): Int {
  return if (a > b) {
    a
  } else {
    b
  }
}

fun simpleMaxOf(a: Int, b: Int) = if (a > b) a else b

val vowels = arrayListOf("a", "e", "i", "o", "u")
fun describe(obj: Any) = when (obj) {
    1          -> "One"
    "Hello"    -> "Greeting"
    is Long    -> "Long"
    !is String -> "Not a string"
    in vowels  -> "Vowel"
    else       -> "Just a string"
}

fun checkUrl(url: String) = when {
    url.startsWith("http://")   -> true
    url.startsWith("https://")  -> true
    else                        -> true
}