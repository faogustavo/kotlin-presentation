package domain.from.project.utils;

import java.util.Calendar;

public class DateUtils {
    public static boolean isSameDay(
            final Calendar cal1, 
            final Calendar cal2
    ) {
      if (cal1 == null || cal2 == null) {
        throw new IllegalArgumentException("The date must not be null");
      }
      return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
              cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
              cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }
}

// Call
DateUtils.isSameDay(date1, date2);