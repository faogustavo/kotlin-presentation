class Empty()

val emptyInstance = Empty()

class InitDemo(name: String) {
    val property = "Property: $name"

    init {
        println("Initializer block that prints $property")
    }
}

val orderInstance = InitDemo("Gustavo")

class AppBusiness(val database: DatabaseConnection) {
  fun loadUsers() : List<User> {
    database.loadUsers()
  }
}

class AppBusiness constructor(val database: DatabaseConnection) {
  fun loadUsers() : List<User> {
    database.loadUsers()
  }
}

class AppBusiness constructor(database: DatabaseConnection) {
    fun loadUsers() : List<User> {
      database.loadUsers() // Error: database is not acessible
    }
}

class AppBusiness @Inject constructor(val database: 
                                        DatabaseConnection) {
    fun loadUsers() : List<User> {
      database.loadUsers()
    }
}

class Person {
    constructor(parent: Person) {
        parent.children.add(this)
    }
}

class Constructors {
    init {
        println("Init block")
    }

    constructor(i: Int) {
        println("Constructor")
    }
}

class Constructors(val i: Int) {
    constructor(i: Int, j: Int) : this(i) {
        println("Constructor")
    }
}