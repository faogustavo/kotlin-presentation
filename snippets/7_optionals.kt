var a: String = "abc"
a = null // compilation error
val al = a.length

var b: String? = "abc"
b = null // ok

val bl = b.length // error: variable 'b' can be null
val bl = if (b != null) b.length else 0

a?.length // 3
b?.length // null

a?.length?.let { println(it) } // prints 3
b?.length?.let { println(it) } // dont enter on the function

b?.length ?: 0  // Elvis operator - Default value
a!!.length // NotNull assertations