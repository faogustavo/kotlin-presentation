import java.util.function.Consumer;

public class Functions {

    void externalFunction() {
        System.out.println("Call of another function");
    }

    public static void main(String[] args) {
        Functions instance = new Functions();

        // Functions
        instance.externalFunction();

        // Function as parameter
        Runnable externalFunction = instance::externalFunction;
        externalFunction.run();

        // Lambdas
        Consumer<String> simpleLambda = (String parameter) -> {
            System.out.println("simpleLambda(\"" + parameter + "\")");
        };
        simpleLambda.accept("Hello World");
    }
}
