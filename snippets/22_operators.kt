data class Point(val x: Int, val y: Int)

// +point
fun Point.unaryPlus() = copy(+x, +y)

// -point
fun Point.unaryMinus() = copy(-x, -y)

// point++
fun Point.inc() = copy(x + 1, y + 1)

// point--
fun Point.dec() = copy(x -1, y - 1)

// point1 + point2
fun Point.plus(other: Point) = copy(x + other.x, y + other.y)

// point1 - point2
fun Point.minus(other: Point) = copy(x - other.x, y - other.y)

// point1 * point2
fun Point.times(other: Point) = copy(x * other.x, y * other.y)

// point1 / point2
fun Point.div(other: Point) = copy(x / other.x, y * other.y)

// point % 2
fun Point.rem(value: Point) = Point(x % value.x, y % value.y)

// point1 == point2
fun Point.equals(other: Any?): Boolean {
    if (other is Point) {
        return x == other.x && y == other.y
    }
    return false
}

// point1 > point2 || point1 >= point2
// point1 < point2 || point1 <= point2
fun Point.compareTo(other: Point): Int {
    val myValue = x + y
    val otherValue = other.x + other.y
    return when {
        myValue < otherValue -> -1
        myValue > otherValue -> 1
        else -> 0
    }
}

// Other operators:
// point += point2 -> Point.plusAssign(other: Point)
// point -= point2 -> Point.minusAssign(other: Point)
// point *= point2 -> Point.timesAssign(other: Point)
// point /= point2 -> Point.divAssign(other: Point)
// point %= point2 -> Point.remAssign(other: Point)
// point .. point2 -> Point.rangeTo(other: Point)