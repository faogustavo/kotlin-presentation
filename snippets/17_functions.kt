fun String.normalizeString(
    normalizeCase: Boolean = true,
    upperCaseFirstLetter: Boolean = true
): String {
    // normalize string (this)
}

"LoReM IpSuM DolOr SIt aMET"
    .normalizeString()

"LoReM IpSuM DolOr SIt aMET"
    .normalizeString(false)

"LoReM IpSuM DolOr SIt aMET"
    .normalizeString(false, false)

// just second parameter
"LoReM IpSuM DolOr SIt aMET"
    .normalizeString(upperCaseFirstLetter = false)