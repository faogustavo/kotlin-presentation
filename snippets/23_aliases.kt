typealias IntList = List<Int>

class A {
    inner class Inner {
        inner class Second {
            fun method() {}
        }
    }
}

typealias ASecond = A.Inner.Second

typealias SimpleFun = () -> Unit

typealias NetworkCallback = (response: NetworkResponse) -> Unit

typealias GenericCallback<I, O> = (I) -> O

inline fun testCallback(parseInt: GenericCallback<String, Int>) {
    parseInt("42")
}

testCallback {
    it.toInt()
}