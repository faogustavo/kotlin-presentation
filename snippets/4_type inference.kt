package main

fun main(args: Array<String>) {
    val v1 = 5
    val v2 = 4
    println("$v1 + $v2 = ${sum(v1, v2)}")
}

fun sum(a: Int, b: Int) = a + b