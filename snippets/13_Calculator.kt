sealed class Expression {
    class Number(val number: Double) : Expression()
    class Sum(val e1: Expression, val e2: Expression) : Expression()
    class Sub(val e1: Expression, val e2: Expression) : Expression()
}

fun eval(e: Expression): Double = when(e) {
    is Expression.Number -> e.number
    is Expression.Sum -> eval(e.e1) + eval(e.e2)
    is Expression.Sub -> eval(e.e1) - eval(e.e2)
}

//Compiler error: with sealed class must be exaustive
fun eval(e: Expression): Double = when(e) {
    is Expression.Number -> e.number
    is Expression.Sum -> eval(e.e1) + eval(e.e2)
}